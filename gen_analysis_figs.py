#!/usr/bin/env python

import json
from matplotlib.legend_handler import HandlerTuple
from matplotlib.legend_handler import HandlerLine2D
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
import math
import pandas

# local import
import utils


#####
##### Global vars
#####

format_dict = {
    "P00":{"marker":"^",                 "color":"#F90101"   , "linewidth":0.75},
    "P11":{"marker":"o","dashes":[1]    ,"color":"orange", "linewidth":2},
    "P22":{"marker":"+","dashes":[3,1,2],"color":"purple"},
    "S00":{"marker":"s",                 "color":"#0266C8"  , "linewidth":0.75},
    "S11":{"marker":"x","dashes":[2, 3] ,"color":"green"},
    "P"    :{"color":"#F90101", "linewidth":0.75},
    "S"    :{"color":"#0266C8", "linewidth":0.75},
    "P (Expe)" :{"marker":"o", "color":"tab:gray", "linewidth":0.75},
    "S (Expe)" :{"marker":"x", "color":"tab:gray", "linewidth":0.75},
    "0.1"  :{"marker":"^"},
    "0.01" :{"marker":"o"},
    "0.001":{"marker":"x"}
}

# margins=dict(bottom=0.25,left=0.27,top=0.97,right=0.98)


#####
##### Utils
#####

def data_from_file(file_name):
    with open(utils.DIR_DATA_ANALYSIS + "/" + file_name) as data_file:
        return json.load(data_file)


#####
##### Plots
#####

def plot_compartment_vs_round(data,comparts,percent=True):
    print("Plotting evolution of "+" ".join(comparts)+" in each round")
    plt.figure(**utils.FIG_SIZE_NO_YLABEL)
    # plt.gcf().subplots_adjust(**margins)
    nb_rounds = 0
    for compart in comparts:
        yvalues = data[compart]
        if percent:
            nodeType = compart[0] # 'P' or 'S'
            total    = data[nodeType+"00"][0]
            yvalues  = [y/total for y in yvalues]
        plt.plot(np.arange(0,len(yvalues)),yvalues,label=compart,markerfacecolor="none",**format_dict[compart])
#                 markerfacecolor="none") #, **format_dict[compart])
        nb_rounds = max(nb_rounds,len(yvalues))

    if percent:
        plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter(1,decimals=0))

    plt.xticks(np.arange(0,nb_rounds))
    plt.legend(loc="center left", bbox_to_anchor=(0.01, 0.5), **utils.FORMAT_LEGEND) #prop={'size': 9})
    plt.xlabel("Rounds")
    # plt.ylabel("Ratio of nodes")
    utils.commonFigFormat(plt.gca())
    # plt.show()


def plot_dissemination_vs_round_density(data, node_type, ax):
    print("Plotting dissemination for density in",data.keys(),".")
    nb_rounds = 10
    for d in sorted(data.keys()):
        yvalues = data[d][node_type][:nb_rounds]
        ax.plot(np.arange(0,len(yvalues)),yvalues, label="d="+str(d),
                 markerfacecolor="none", **format_dict[node_type], **format_dict[d])

    ax.yaxis.set_major_formatter(mtick.PercentFormatter(1,decimals=0))
    ax.set_xticks(np.arange(0,nb_rounds))
    ax.set_xlabel("Rounds")
    ax.set_ylabel("CDF infected nodes")
    utils.commonFigFormat(ax)


def plot_one_dissemination_vs_round_density(data, node_type):
    plt.figure(**utils.FIG_SIZE)
    # plt.gcf().subplots_adjust(**margins)
    plot_dissemination_vs_round_density(data, node_type, plt.gca())
    plt.legend(loc="upper left", **utils.FORMAT_LEGEND)


def plot_both_dissemination_vs_round_density(data):
    fig, ax = plt.subplots(1, 2, **utils.FIG_SIZE_DOUBLE)
    plot_dissemination_vs_round_density(data, "P", ax[0])
    plot_dissemination_vs_round_density(data, "S", ax[1])

    ax[1].spines['left'].set_visible(False)
    ax[1].set_ylabel("")
    ax[1].set_yticklabels([])

    # ax[1].legend(loc="upper center", ncol=1, bbox_to_anchor=(0.00, 0.75), **utils.FORMAT_LEGEND)

    ### Change marker color in legend
    ### https://stackoverflow.com/questions/48391146/change-marker-in-the-legend-in-matplotlib
    def update_prop(handle, orig):
        handle.update_from(orig)
        handle.set_color("black")

    ax[1].legend(loc="upper center", ncol=1, bbox_to_anchor=(0.00, 0.75), **utils.FORMAT_LEGEND,
        handler_map={plt.Line2D:HandlerLine2D(update_func=update_prop)})


def plot_vs_density_for_P_and_S(data,metric,ylabel=None,ymin=None,left_margin=0.2):
    print("Plotting "+metric+" against density")
    plt.figure(**utils.FIG_SIZE)
    # plt.gcf().subplots_adjust(**margins)
    for node_type in ["P","S"]:
        plt.plot(data["density"],data[metric][node_type],label=node_type,
                 markerfacecolor="none", **format_dict[node_type+"00"])
    plt.legend(loc="lower left", **utils.FORMAT_LEGEND) #prop={'size': 9})
    plt.xlabel("Density")
    plt.gca().set_xscale('log')

    if not ylabel:
        ylabel = metric.title() 
    plt.ylabel(ylabel)
    plt.gca().set_ylim([ymin,None])
    utils.commonFigFormat(plt.gca())


def plot_std_latency_vs_latency_for_P_and_S(data):
    print("Plotting std_latency vs latency for P and S")
    plt.figure(**utils.FIG_SIZE)
    # plt.gcf().subplots_adjust(bottom=0.25,left=0.2,top=0.97,right=0.98)
    for node_type in ["P","S"]:
        plt.plot(data["latency"][node_type],data["std_latency"][node_type],label=node_type,
                 markerfacecolor="none", **format_dict[node_type+"00"])
    plt.legend(loc="lower left", handlelength=1, **utils.FORMAT_LEGEND)
    plt.xlabel("Latency")

    plt.ylabel("Latency standard deviation", loc="top")
    plt.gca().set_ylim([0.4,0.8])
    plt.gca().set_xlim([0,7])
    plt.gca().xaxis.set_major_locator(mtick.MultipleLocator(1))

    utils.commonFigFormat(plt.gca())

    label_pos_P = {
        0.001:(-45,17),
        0.01 :(-18,25),
        0.1  :( 0,15)
    }

    label_pos_S = {
        0.001:(-53,-22),
        0.01 :(-53,-35),
        0.1  :(-53.5,-25)
    }

    format = {"xycoords":"data", #"fontsize":7.5,
              "arrowprops":dict(arrowstyle="->",lw=0.65),"textcoords":'offset points'}
    
    for d in [0.001,0.01,0.1]:
        index = data["density"].index(d)
        plt.annotate("d="+str(d),
                     xy=(data["latency"]["P"][index], data["std_latency"]["P"][index]),
                     xytext=label_pos_P[d], **format)
        plt.annotate("d="+str(d),
                     xy=(data["latency"]["S"][index], data["std_latency"]["S"][index]),
                     xytext=label_pos_S[d], **format)


def plot_inconsistency_vs_round(theo_data,expe_data):
    """ One figure for both primary and secondary """
    print("Plotting inconsistency by round")
    plt.figure(**utils.FIG_SIZE)
    # plt.gcf().subplots_adjust(bottom=0.2,left=0.22,top=0.97,right=0.98)
    nb_rounds = 0

    for node_type in ["P","S"]:
        yvalues = theo_data[node_type]
        plt.plot(np.arange(0,len(yvalues)),yvalues,label=node_type+" (Theory)",
                 markerfacecolor="none",**format_dict[node_type+"00"])
        nb_rounds = max(nb_rounds,len(yvalues))
        
    for node_type in ["P","S"]:
        name = node_type+" (Expe)"
        plt.plot(np.arange(0,nb_rounds),[y/100 for y in expe_data[node_type+" mean"][0:nb_rounds]],
                 label=name, markerfacecolor="none", dashes=[3, 3], **format_dict[name])

    ax = plt.gca()
    ax.yaxis.set_major_formatter(mtick.PercentFormatter(1,decimals=0))
    ax.set_ylim([None,0.1])
    plt.xticks([x*2 for x in np.arange(0,math.ceil(nb_rounds/2))])
    ax.set_xlim([2,18])

    plt.xlabel("Rounds")
    plt.ylabel("Ratio of inconsistent nodes", loc="top")
    # plt.legend(loc="upper right", ncol=2, columnspacing=1, **utils.FORMAT_LEGEND)

    ### Merging lines in legend
    ### https://matplotlib.org/3.3.3/tutorials/intermediate/legend_guide.html#legend-handlers
    plt.legend([(ax.get_lines()[0], ax.get_lines()[2]),  # first tuple
                (ax.get_lines()[1], ax.get_lines()[3])], # second tuple
               ['P: theory & expe', 'S: theory & expe'],
        handler_map={tuple: HandlerTuple(ndivide=None)},
        fontsize=utils.FONT_SIZE_S, loc="upper center", **utils.FORMAT_LEGEND)

    utils.commonFigFormat(plt.gca())


#####
##### Main
#####

if __name__ == "__main__":
    utils.init()

    plot_data = data_from_file("comparts_by_round_d10-3.json")
    plot_compartment_vs_round(plot_data,["P00","P11","P22"],"%Primary nodes")
    utils.saveFig("analysis_primaries_10-3")

    plot_data = data_from_file("comparts_by_round_d10-3.json")
    plot_compartment_vs_round(plot_data,["S00","S11"],"%Secondary nodes")
    utils.saveFig("analysis_secondaries_10-3")

    plot_data = data_from_file("comparts_by_round_d10-3.json")
    plot_compartment_vs_round(plot_data,["P00","P11","P22","S00","S11"])
    utils.saveFig("analysis_all_10-3")

    plot_data = data_from_file("dissemination_by_round_by_density.json")
    plot_one_dissemination_vs_round_density(plot_data,"P")
    utils.saveFig("analysis_dissem_by_density_P")

    plot_data = data_from_file("dissemination_by_round_by_density.json")
    plot_one_dissemination_vs_round_density(plot_data,"S")
    utils.saveFig("analysis_dissem_by_density_S")

    plot_data = data_from_file("dissemination_by_round_by_density.json")
    plot_both_dissemination_vs_round_density(plot_data)
    utils.saveFig("analysis_dissem_by_density_both")

    plot_data = data_from_file("lat_std_lat_vs_density_data.json")
    plot_vs_density_for_P_and_S(plot_data,"latency",ymin=0,left_margin=0.15)
    utils.saveFig("latency_vs_density_P_and_S")

    plot_data = data_from_file("lat_std_lat_vs_density_data.json")
    plot_vs_density_for_P_and_S(plot_data,"std_latency","Std dev latency",ymin=0.4)
    utils.saveFig("std_dev_latency_vs_density_P_and_S")

    plot_data = data_from_file("lat_std_lat_vs_density_data.json")
    plot_std_latency_vs_latency_for_P_and_S(plot_data)
    utils.saveFig("std_dev_latency_vs_latency_P_and_S")

    plot_theo_data = data_from_file ("inconsistency_vs_round_10minus1.json")
    plot_expe_data = pandas.read_csv(utils.DIR_DATA_ROUNDS +  "/dup_0.1d.consistencies.csv")
    plot_inconsistency_vs_round(plot_theo_data,plot_expe_data)
    utils.saveFig("inconsistency_vs_round_10minus1")
