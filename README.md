
# GPS/UPS plots

Python3 scripts to generate uniformized analytical and experimental plots for the GPS/UPS paper.


## Dependencies

* matplotlib
* numpy
* pandas


## Submodules

* [Experimental results](https://gitlab.inria.fr/WIDE/gps-exp)


## SRDS 2016 figures

In directory `experiments/results/015_dissemination_secondary/`:

* Fig 5a: obtained from the points in Fig 5b and 5c, cf. Fig 9 of journal version
* Fig 5b: `minConsistencies_gps.pdf`
* Fig 5c: `latencies_gps.pdf`
* Fig 6a: `consistencies_gps_0.1d.pdf`
* Fig 6b: `consistencies_gps_0.01d.pdf`
* Fig 6c: `consistencies_gps_0.001d.pdf`
* Fig 7a: `dissemination_gps_0.1d.pdf`
* Fig 7b: `dissemination_gps_0.01d.pdf`
* Fig 7c: `dissemination_gps_0.001d.pdf`


## Journal figures

* Fig 6: `analysis_dissem_by_density_both.pdf`
* Fig 7: `analysis_all_10-3.pdf`
* Fig 8: `std_dev_latency_vs_latency_P_and_S.pdf`
* Fig 9: `expe_summary.pdf`
* Fig 10: `expe_consistency.pdf`
* Fig 11: `expe_jitter.pdf`
* Fig 12: `inconsistency_vs_round_10minus1.pdf`
* Fig 13: `expe_dissemination.pdf`
* Fig 14: `expe_blockchain_consistency.pdf`
* Fig 15: `expe_blockchain_dissemination.pdf`
