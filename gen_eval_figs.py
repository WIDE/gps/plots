#!/usr/bin/env python3

import math
import matplotlib.cbook as cbook
import matplotlib.colors as col
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import pandas as pd
import re
import string

# local import
import utils


#####
##### Global vars are for lazy people
#####

BAR_FORMAT = {
    # "Uniform":     {"hatch": "//", "facecolor": "white", "edgecolor": "black"},
    "UPS d=0.001": {"hatch": "//", "facecolor": "white", "edgecolor": "tab:green"},
    "UPS d=0.01":  {"hatch": "xx", "facecolor": "white", "edgecolor": "tab:blue"},
    "UPS d=0.1":   {"hatch": "O",  "facecolor": "white", "edgecolor": "red"},
}

LINE_FORMAT = {
    "Uniform": {"marker": "+", "linestyle": "dashed", "color": "black"},
    "0.001":   {"marker": "s", "linestyle": "dotted", "color": "tab:green"},
    "0.01":    {"marker": "o", "linestyle": "dotted", "color": "tab:blue"},
    "0.1":     {"marker": "^", "linestyle": "dotted", "color": "red"},
}

# Confidence interval computation
# https://stackoverflow.com/questions/53519823/confidence-interval-in-python-dataframe/53522680#53522680
CI_LEVEL = 0.95


#####
##### Plot templates
#####

# Unused, stopped half-way
def manualBoxPlot():
    boxes = list(range(0, len(csv["round"])))
    for i in boxes:
        box = {
            'label' : "{}".format(i),
            'mean'  : csv["Uniform mean"][i],
            'iqr'   : None,
            'cilo'  : None,
            'cihi'  : None,
            'whishi': csv["Uniform max"][i],
            'whislo': csv["Uniform min"][i],
            'fliers': None,
            'q1'    : csv["Uniform 25th"][i],
            'med'   : csv["Uniform 50th"][i],
            'q3'    : csv["Uniform 75th"][i],
        }
        ax.bxp(box)
    # print(boxes)


    ### Generate useless data but with correct naming
    np.random.seed(1)
    data = np.random.lognormal(size=(37, 4), mean=1.5, sigma=1.75)
    labels = list('ABCD')
    stats = cbook.boxplot_stats(data, labels=labels, bootstrap=10000)
    # print(stats)


def barplot(data, ax):
    xTicksLabels = list(list(data.values())[0].keys())

    ### Bar locations and width
    xTicks = np.arange(len(xTicksLabels)) # label locations
    nbBars = len(data.keys())
    width = 1.0 / (nbBars + 1) # 2 sets: 0.33 width, 3 sets: 0.25, 4: 0.2, ...
    location = lambda i: xTicks + (i-nbBars/2+0.5)*width

    bars = {}
    for i, approach in enumerate(data.keys()):
        bars[approach] = ax.bar(location(i), data[approach].values(), width,
            label=approach, fill=True, **BAR_FORMAT[approach])

    ### Ticks, limits
    ax.set_xticks(xTicks)
    ax.set_xticklabels(xTicksLabels)

    ### Grid y only
    ax.grid(which='both', axis='x', linestyle='')
    # ax.grid(which='both', axis='y', linestyle='--')
    ax.set_axisbelow(True)

    return ax


#####
##### Plots
#####

def plotOneConsistency(ax, subset):
    utils.commonFigFormat(ax)
    lines = {}

    # 0: file, 1: column, 2: label, 3: line_format id
    for key in [
                ("uniform", "Uniform", "Uniform", "Uniform"),
                ("dup_0.001d", subset, "UPS d=0.001", "0.001"),
                ("dup_0.01d", subset, "UPS d=0.01", "0.01"),
                ("dup_0.1d", subset, "UPS d=0.1", "0.1"),
                ]:
        csv = pd.read_csv(utils.DIR_DATA_ROUNDS + "/" + key[0] + ".consistencies.csv", index_col=0)
        x = csv.index.values
        y = csv[key[1]+" mean"]
        # y = csv[key[1]+" 50th"]
        yerrmin = y - csv[key[1]+" min"]
        yerrmax = csv[key[1]+" max"] - y
        # yerr = (1 + CI_LEVEL) * csv[key[1]+" cistd"]
        lines[key[3]] = ax.errorbar(x, y, #yerr=yerr,
            yerr=[yerrmin, yerrmax],
            label=key[2], markerfacecolor="none", **LINE_FORMAT[key[3]])

    ### Title, labels
    ax.set_xlabel("Rounds")
    ax.set_ylabel("Ratio of inconsistent nodes", loc="top")

    ### Axes bounds and ticks
    ax.set_xlim([2, 18])
    ax.set_ylim([None, 10])
    ax.xaxis.set_major_locator(ticker.MultipleLocator(2))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(2))
    ax.yaxis.set_major_formatter(ticker.PercentFormatter(100, decimals=0))

    return lines


def plotBothConsistency():
    fig, ax = plt.subplots(1, 2, **utils.FIG_SIZE_DOUBLE)
    linesA = plotOneConsistency(ax[0], "P")
    linesB = plotOneConsistency(ax[1], "S")

    ax[1].spines['left'].set_visible(False)
    ax[1].set_ylabel("")
    ax[1].set_yticklabels([])

    ### Single legend in the middle
    ax[1].legend(loc="upper center", ncol=2, bbox_to_anchor=(-0.1, 1), **utils.FORMAT_LEGEND)
        # borderaxespad=1, columnspacing=0.5, handletextpad=0.5, labelspacing=0.1)

    ### Legend split in two
    # ax[0].legend(
    #     (linesA['Uniform'], linesA['0.1']),
    #     (linesA['Uniform'].get_label(), linesA['0.1'].get_label()),
    #     loc="upper center", ncol=1, bbox_to_anchor=(0.7, 1.044), **utils.FORMAT_LEGEND)
    # ax[1].legend(
    #     (linesA['0.01'], linesA['0.001']),
    #     (linesA['0.01'].get_label(), linesA['0.001'].get_label()),
    #     loc="upper center", ncol=1, bbox_to_anchor=(0.35, 1.044), **utils.FORMAT_LEGEND)

    utils.saveFig("expe_consistency")


def plotOneLatency(ax, subset):
    utils.commonFigFormat(ax)
    lines = {}

    # 0: file, 1: column, 2: label, 3: line_format id
    for key in [
                ("uniform", "Uniform", "Uniform", "Uniform"),
                ("dup_0.001d", subset, "UPS d=0.001", "0.001"),
                ("dup_0.01d", subset, "UPS d=0.01", "0.01"),
                ("dup_0.1d", subset, "UPS d=0.1", "0.1"),
                ]:
        csv = pd.read_csv(utils.DIR_DATA_ROUNDS + "/" + key[0] + ".dissemination.csv", index_col=0)
        x = csv.index.values
        # y = csv[key[1]+" 50th"]
        # yerrmin = y - csv[key[1]+" 05th"]
        # yerrmax = csv[key[1]+" 95th"] - y
        y = csv[key[1]+" mean"]
        yerrmin = y - csv[key[1]+" min"]
        yerrmax = csv[key[1]+" max"] - y
        # yerr = (1 + CI_LEVEL) * csv[key[1]+" cistd"]
        lines[key[3]] = ax.errorbar(x, y, #yerr=yerr,
            yerr=[yerrmin, yerrmax],
            label=key[2], markerfacecolor="none", **LINE_FORMAT[key[3]])

    ### Title, labels
    ax.set_xlabel("Rounds")
    ax.set_ylabel("CDF fully infected nodes", loc="top")

    ### Axe bounds and ticks
    ax.set_xlim([8, 18])
    # ax.set_ylim([0, 100])
    ax.xaxis.set_major_locator(ticker.MultipleLocator(2))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(20))
    ax.yaxis.set_major_formatter(ticker.PercentFormatter(100, decimals=0))

    return lines


def plotBothLatency():
    fig, ax = plt.subplots(1, 2, **utils.FIG_SIZE_DOUBLE)
    linesA = plotOneLatency(ax[0], "P")
    linesB = plotOneLatency(ax[1], "S")

    ax[1].spines['left'].set_visible(False)
    ax[1].set_ylabel("")
    ax[1].set_yticklabels([])

    ax[1].legend(loc="upper center", ncol=1, bbox_to_anchor=(0.00, 0.82), **utils.FORMAT_LEGEND)
        # borderaxespad=1, columnspacing=0.5, handletextpad=0.5, labelspacing=0.1)

    utils.saveFig("expe_dissemination")


def plotJitter():
    fig, ax = plt.subplots(1, 1, **utils.FIG_SIZE)
    utils.commonFigFormat(ax)

    data = {
        "UPS d=0.001": {},
        "UPS d=0.01":  {},
        "UPS d=0.1":   {},
    }

    for d in ["0.001", "0.01", "0.1"]:
        csv = pd.read_csv(utils.DIR_DATA_ROUNDS + "/dup_" + d + "d.latencies.csv", index_col=0)
        data["UPS d="+d]["Primary"] = csv["std"]["P"]
        data["UPS d="+d]["Secondary"] = csv["std"]["S"]

    ax = barplot(data, ax)

    csv = pd.read_csv(utils.DIR_DATA_ROUNDS + "/uniform.latencies.csv", index_col=0)
    ax.axhline(y=csv["std"]["Uniform"], label='Uniform', color='black', linestyle="--", zorder=-1)

    ax.set_xlabel("Node class")
    ax.set_ylabel("Latency standard deviation", loc="top")
    ax.legend(loc="lower center", #bbox_to_anchor=(0.5, 0.),
        handlelength=1.5, #edgecolor="black",
        fontsize=utils.FONT_SIZE_S, **utils.FORMAT_LEGEND)

    # ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.05))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
    ax.set_ylim([0, 0.73])

    utils.saveFig("expe_jitter")


def plotExperimentsSummary():
    fig, ax = plt.subplots(1, 1, **utils.FIG_SIZE_DOUBLE)
    utils.commonFigFormat(ax)

    ### Need to redefine an empty linestyle so we can't use the global dict as is
    helper = lambda x: {"linestyle": "none",
        "marker": LINE_FORMAT[x]["marker"], "color": LINE_FORMAT[x]["color"]}

    ax.plot(6.24, 4.62, label="Uniform", **helper("Uniform"))
    ax.plot(3.23, 5.16, label="P 0.001", **helper("0.001"))
    ax.plot(4.24, 4.84, label="P 0.01",  **helper("0.01"))
    ax.plot(5.24, 4.66, label="P 0.1",   **helper("0.1"))
    ax.plot(6.63, 4.01, label="S 0.001", **helper("0.001"), markerfacecolor="none")
    ax.plot(6.63, 3.03, label="S 0.01",  **helper("0.01"),  markerfacecolor="none")
    ax.plot(6.69, 1.02, label="S 0.1",   **helper("0.1"),   markerfacecolor="none")

    ### Add a vertical and horizontal line to project the Uniform point
    ax.plot([0, 6.24, 6.24], [4.62, 4.62, 0], linestyle="dotted", linewidth=1.5,
        color="black", zorder=-1)

    ### Title, labels
    ax.set_xlabel("Rounds")
    ax.set_ylabel("Maximum ratio of\ninconsistent nodes", loc="center")

    ### Axe bounds and ticks
    ax.set_xlim([0, 11])
    ax.set_ylim([0, 8])
    ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.yaxis.set_major_formatter(ticker.PercentFormatter(100, decimals=0))

    ax.legend(loc="center right", ncol=1, **utils.FORMAT_LEGEND)

    utils.saveFig("expe_summary")


##########
########## Blockchain figures
##########

def plotBlockchainOneAxis(ax, fileSuffix, delayFactor, xlim, markPeriod=25):
    utils.commonFigFormat(ax)

    # 0: file, 1: column, 2: label, 3: line_format id
    for key in [
                ("3_uniform_real-topo", "Uniform", "Uniform", "Uniform"),
                ("1_gps_real-topo", "P", "GPS: P", "0.1"),
                ("1_gps_real-topo", "S", "GPS: S", "0.01"),
                ]:
        csv = pd.read_csv(utils.DIR_DATA_BLOCKCHAIN + "/" + delayFactor + "/" + key[0] + fileSuffix, index_col=0)
        x = csv.index.values
        y = csv[key[1]+" mean"]
        yerr = (1 + CI_LEVEL) * csv[key[1]+" cistd"]
        # yerr = csv[key[1]+" std"]
        # yerrmin = y - csv[key[1]+" min"]
        # yerrmax = csv[key[1]+" max"] - y
        ### sampling
        # sample = 5
        # x = x[::sample]
        # y = y.to_numpy()[::sample]
        # yerr = yerr.to_numpy()[::sample]
        ax.errorbar(x, y,
            # yerr=[yerrmin, yerrmax],
            yerr=yerr,
            label=key[2],
            errorevery=markPeriod,
            markevery=markPeriod,
            rasterized=False, # 2022-06: with high enough markPeriod, no need to rasterize
            markerfacecolor="none",
            **LINE_FORMAT[key[3]])

    ax.set_xlim(xlim)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(500))
    # ax.xaxis.set_major_formatter(lambda x, pos: round(x/(1000)))
    ax.xaxis.set_major_formatter(lambda x, pos: x/(1000))


def plotBlockchainConsistency():
    fileSuffix = '.consistencies.csv'
    fig, ax = plt.subplots(1, 3, **utils.FIG_SIZE_DOUBLE)
    # plotBlockchainOneAxis(ax[0], fileSuffix, "0.002", [0*1000, 1.1*1000])
    plotBlockchainOneAxis(ax[0], fileSuffix, "0.005", [0*1000, 1.6*1000])
    plotBlockchainOneAxis(ax[1], fileSuffix, "0.01", [0*1000, 2.1*1000])
    # plotBlockchainOneAxis(ax[2], fileSuffix, "0.02", [0*1000, 4.1*1000])
    # plotBlockchainOneAxis(ax[2], fileSuffix, "0.05", [0*1000, 9.1*1000])
    plotBlockchainOneAxis(ax[2], fileSuffix, "0.1", [0*1000, 21*1000], markPeriod=500)
    # plotBlockchainOneAxis(ax[2], fileSuffix, "0.5", [60.0*1000, 80.1*1000])

    ax[1].set_xlabel("Time [s]")
    ax[0].set_ylabel("Ratio of inconsistent nodes", loc="top")

    ax[0].yaxis.set_major_formatter(ticker.PercentFormatter(100, decimals=0))
    for i in range(0, len(ax)):
        ax[i].set_ylim([0, 5.2])
        ax[i].set_yticks([0, 1, 2, 3, 4])

    for i in range(1, len(ax)):
        ax[i].xaxis.set_minor_locator(ticker.MultipleLocator(500))
        ax[i].xaxis.set_major_locator(ticker.MultipleLocator(1000))
        ax[i].xaxis.set_major_formatter(lambda x, pos: round(x/(1000)))
    ax[2].xaxis.set_major_locator(ticker.MultipleLocator(5000))
    ax[2].xaxis.set_minor_locator(ticker.MultipleLocator(5000))

    for i in range(1, len(ax)):
        ax[i].spines['left'].set_visible(False)
        ax[i].set_yticklabels([])

    ax[2].legend(ncol=3, loc="upper center", bbox_to_anchor=(-0.7, 1.07),
        borderaxespad=1, #columnspacing=1, #labelspacing=0.1,
        **utils.FORMAT_LEGEND)

    utils.saveFig("expe_blockchain_consistency")


def plotBlockchainLatency():
    fileSuffix = '.dissemination.csv'
    fig, ax = plt.subplots(1, 3, **utils.FIG_SIZE_DOUBLE)
    # plotBlockchainOneAxis(ax[0], fileSuffix, "0.002", [0*1000, 1.1*1000])
    plotBlockchainOneAxis(ax[0], fileSuffix, "0.005", [0.5*1000, 1.6*1000])
    plotBlockchainOneAxis(ax[1], fileSuffix, "0.01", [1.0*1000, 2.6*1000])
    # plotBlockchainOneAxis(ax[2], fileSuffix, "0.02", [2.4*1000, 4.1*1000])
    # plotBlockchainOneAxis(ax[2], fileSuffix, "0.05", [6.0*1000, 9.1*1000])
    plotBlockchainOneAxis(ax[2], fileSuffix, "0.1", [11.0*1000, 17.4*1000], markPeriod=100)
    # plotBlockchainOneAxis(ax[2], fileSuffix, "0.5", [60.0*1000, 80.1*1000])

    ax[1].set_xlabel("Time [s]")
    ax[0].set_ylabel("CDF fully infected nodes", loc="top")

    ax[0].yaxis.set_major_formatter(ticker.PercentFormatter(100, decimals=0))
    for i in range(0, len(ax)):
        ax[i].set_ylim([0, 130])
        ax[i].set_yticks([0, 25, 50, 75, 100])

    ax[2].xaxis.set_major_formatter(lambda x, pos: round(x/(1000)))
    ax[2].set_xticks([11*1000, 13*1000, 15*1000, 17*1000])

    for i in range(1, len(ax)):
        ax[i].spines['left'].set_visible(False)
        ax[i].set_yticklabels([])

    ax[2].legend(ncol=3, loc="upper center", bbox_to_anchor=(-0.7, 1.07),
        borderaxespad=1, #columnspacing=1, #labelspacing=0.1,
        **utils.FORMAT_LEGEND)

    utils.saveFig("expe_blockchain_dissemination")


#####
##### Main
#####

if __name__ == "__main__":
    utils.init()
    plotBothConsistency()
    plotBothLatency()
    plotJitter()
    plotExperimentsSummary()
    plotBlockchainConsistency()
    plotBlockchainLatency()
