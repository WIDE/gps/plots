#!/usr/bin/env python

import json
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
import math
import pandas

# This computes measures of discrepencies between the theory and
# experiments for the files "inconsistency_vs_round_10minus1.json" and
# "../figs_eval/data/dup_0.1d.consistencies.csv"

def data_from_file(file_name):
    with open(file_name) as data_file:
        return json.load(data_file)

theo_data = data_from_file ("inconsistency_vs_round_10minus1.json")
expe_data = pandas.read_csv("../figs_eval/data/dup_0.1d.consistencies.csv")

for node_type in ["P","S"]:
    theo_incons = theo_data[node_type]
    nb_rounds   = len(theo_incons)
    expe_incons = [y/100 for y in expe_data[node_type+" mean"][0:nb_rounds]]

    err_array   = [abs(theo-expe) for (theo,expe) in zip(theo_incons,expe_incons)]
    max_abs_err = max(err_array)
    print("max abs error for "+node_type+":"+str(max_abs_err*100)+"%")
    rounds = np.argmax(err_array)
    print("observed in rounds "+str(rounds))
    print("mean absolute error: "+str(np.mean(err_array)*100)+"%")
